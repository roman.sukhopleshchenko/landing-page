const show = (state) => {
  switch(state.windowToShow) {
    case 'menu':
      state.elements.contactsWindow.classList.remove('show');
      state.elements.menuWindow.classList.add('show');
      break;
    case 'contacts':
    state.elements.menuWindow.classList.remove('show');
    state.elements.contactsWindow.classList.add('show');
    break;
    default:
      state.elements.menuWindow.classList.remove('show');
      state.elements.contactsWindow.classList.remove('show');
  }
}

const viewScript = () => {
  const state = {
    windowToShow: null,
    elements: {
      menuBtn: document.querySelector('#menuBtn'),
      menuWindow: document.querySelector('#menu'),
      contactsLink: document.querySelector('#contactsLink'),
      contactsWindow: document.querySelector('#contacts'),
      essenceLink: document.querySelector('#essenceLink'),
      conditionsLink: document.querySelector('#conditionsLink'),
  }
};

  state.elements.menuBtn.addEventListener('click', () => {
    if (state.windowToShow === null) {
      state.windowToShow = 'menu';
    } else {
      state.windowToShow = null;
    }
    show(state);
  });

  state.elements.contactsLink.addEventListener('click', () => {
    state.windowToShow = 'contacts';
    show(state);
  });

  state.elements.essenceLink.addEventListener('click', () => {
    state.windowToShow = null;
    show(state);
  });

  state.elements.conditionsLink.addEventListener('click', () => {
    state.windowToShow = null;
    show(state);
  });

};

viewScript();
